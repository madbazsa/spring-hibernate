package mb.hibernate.components;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Profile;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/*
    PROFILE:  aktiválni az application.properties fájlba tudom: spring.profiles.active=logger
              config fájl: application-<PROFILE>.properties
*/
@Profile("logger")
@Component
public class LoggerBean {
    private Logger logger = LoggerFactory.getLogger(this.getClass());
    
    @Value("${logger.message}")
    private String msg;
    
    // Lefut minden perc 10, 20, stb másodpercébe. (application-logger.properties fájlba van)
    @Scheduled(cron="${logger.corn}")
    public void cronJob() {
        logger.info("cronJob!!!!" + msg);
    }
    
}
