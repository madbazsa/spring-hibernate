package mb.hibernate.components;

import static java.lang.System.exit;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import mb.hibernate.domain.Address;
import mb.hibernate.domain.Credential;
import mb.hibernate.domain.User;
import mb.hibernate.repository.CredentialRepository;
import mb.hibernate.repository.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Profile("jpa")
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
@Component
public class JpaRunner implements CommandLineRunner {

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    UserRepository userRepository;

    @Autowired
    CredentialRepository credentialRepository;

    @Profile("jpa")
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    @Override
    public void run(String... args) throws Exception {
        DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

        /* Delete all user */
        this.userRepository.deleteAllInBatch();

        /* Add new user */
        User user1 = new User();
        user1.setName("Mad Bazsa");
        user1.setCreatedDate(new Date());
        user1.setBirthDay(sdf.parse("1986-04-13"));

        user1.setPostAddress(new Address("7655", "Pécs", "Mártírok"));
        user1.setBankAddress(new Address("7631", "Pécs", "Zsolnai"));

        user1.getEmails().add("tesat@mb.hu");
        user1.getEmails().add("test2@mb.hu");

        user1.getPhoneNumbers().put("MOBILE", "123456");
        user1.getPhoneNumbers().put("PHONE", "646577");

        user1.getPrevAddresses().add(new Address("1111", "Debrecen", "Rácz"));
        user1.getPrevAddresses().add(new Address("222", "Bp", "Hungária"));

        /* OneToOne */
        Credential cred1 = new Credential("madbazsa", "madbazsa1");
        cred1.setUser(user1);
        this.credentialRepository.save(cred1);

        User newUser = this.userRepository.findByName("Mad Bazsa");
        newUser.setValid(true);
        logger.info("USER: " + newUser);

        Credential newCred = this.credentialRepository.findByUsername("madbazsa");
        logger.info("CRED: " + newCred.getUser());
        
        logger.info(this.userRepository.getName(new Long(1)));

        exit(0);
    }

}
