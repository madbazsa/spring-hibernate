package mb.hibernate.repository;

import mb.hibernate.domain.Credential;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CredentialRepository extends JpaRepository<Credential, Long> {
    Credential findByUsername(String username);
}
