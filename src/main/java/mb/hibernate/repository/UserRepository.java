package mb.hibernate.repository;

import mb.hibernate.domain.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.query.Procedure;
import org.springframework.data.repository.query.Param;

public interface UserRepository extends JpaRepository<User, Long> {
    User findByName(String name);
    
    @Procedure(name = "getName")
    String getName(@Param("p_id") Long inParam1);
}
