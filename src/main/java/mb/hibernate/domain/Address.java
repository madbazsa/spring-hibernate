package mb.hibernate.domain;

import javax.persistence.Column;
import javax.persistence.Embeddable;

// Beagyazható másik entitásba, nem lesz külön tábla, része lesz a szülő táblának.
// Nincs Entity és Id annotáció, mert ez "része" a User entitásnak.
@Embeddable
public class Address {
    @Column
    private String zip;
    
    @Column
    private String city;
    
    @Column
    private String street;

    public String getZip() {
        return zip;
    }

    public void setZip(String zip) {
        this.zip = zip;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    @Override
    public String toString() {
        return "Address{" + "zip=" + zip + ", city=" + city + ", street=" + street + '}';
    }

    public Address(String zip, String city, String street) {
        this.zip = zip;
        this.city = city;
        this.street = street;
    }

    public Address() {
    }
    
    
}
