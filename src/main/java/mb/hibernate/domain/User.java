package mb.hibernate.domain;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.MapKeyColumn;
import javax.persistence.NamedStoredProcedureQueries;
import javax.persistence.NamedStoredProcedureQuery;
import javax.persistence.OneToOne;
import javax.persistence.ParameterMode;
import javax.persistence.StoredProcedureParameter;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import org.hibernate.annotations.Formula;

@Entity
@NamedStoredProcedureQueries({
   @NamedStoredProcedureQuery(name = "getName", 
                              procedureName = "get_name",
                              parameters = {
                                 @StoredProcedureParameter(mode = ParameterMode.IN, name = "p_id", type = Long.class),
                                 @StoredProcedureParameter(mode = ParameterMode.OUT, name = "p_name", type = String.class)
                              })
})
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    
    @Column
    private String name;
    
    @Column(name="created_date", updatable=false)
    @Temporal(TemporalType.TIMESTAMP)    // milyen formában akarjuk visszakapni a dátumot
    private Date createdDate;
    
    @Column
    @Temporal(TemporalType.DATE)
    private Date birthDay;
    
    /* Adatbázisba számolt érték, nem létezik a mező az adatbázisba */
    @Formula("lower(datediff(curdate(), birth_day)/365)")
    private int age;
    
    /* Átmeneti változó, adatbázisba nem létezik */
    @Transient
    private boolean valid;
    
    @Embedded // A User táblába lesznek az Address mezői, nem külön tábla.
    private Address postAddress = new Address();
    
    @Embedded
    // Az Address entity-t felhasználjuk, és megadjuk melyik mezőt milyen néven mentsen el a User táblába.
    @AttributeOverrides({
        @AttributeOverride(name="zip", column=@Column(name="bank_zip")),
        @AttributeOverride(name="city", column=@Column(name="bank_city")),
        @AttributeOverride(name="street", column=@Column(name="bank_street")),
    })
    private Address bankAddress = new Address();
    
    // Egy új táblába (USER_EMAIL) lesznek a emailek (EMAIL) és a (USER_ID) mint idegenkulcs.
    @ElementCollection
    @CollectionTable(name="USER_EMAIL", joinColumns=@JoinColumn(name="USER_ID"))
    @Column(name="EMAIL")
    private List<String> emails = new ArrayList<String>();
    
    // Egy új táblába (USER_PHONE) lesznek a telefonszámok (PHONE_NUMBER) és a tipus a (PHONE_TYPE) és a (USER_ID) mint idegenkulcs. 
    @ElementCollection
    @CollectionTable(name="USER_PHONE", joinColumns=@JoinColumn(name="USER_ID"))
    @MapKeyColumn(name="PHONE_TYPE")
    @Column(name="PHONE_NUMBER")
    private Map<String, String> phoneNumbers = new HashMap<String, String>();

    // Egy új táblába (PREVIOUS_ADDRESSES) lesznek a címek, hozzárendelve a USER_ID
    @ElementCollection
    @CollectionTable(name="PREVIOUS_ADDRESSES", joinColumns=@JoinColumn(name="USER_ID"))
    private List<Address> prevAddresses = new ArrayList<Address>();
    
    // Ez a változó csak akkor szükséges, ha mindkét irányból elakarjuk érni a másik objektumot.
    @OneToOne(mappedBy="user")
    private Credential credential;
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Date getBirthDay() {
        return birthDay;
    }

    public void setBirthDay(Date birthDay) {
        this.birthDay = birthDay;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public boolean isValid() {
        return valid;
    }

    public void setValid(boolean valid) {
        this.valid = valid;
    }

    public Address getPostAddress() {
        return postAddress;
    }

    public void setPostAddress(Address postAddress) {
        this.postAddress = postAddress;
    }

    public Address getBankAddress() {
        return bankAddress;
    }

    public void setBankAddress(Address bankAddress) {
        this.bankAddress = bankAddress;
    }

    public List<String> getEmails() {
        return emails;
    }

    public void setEmails(List<String> emails) {
        this.emails = emails;
    }

    public Map<String, String> getPhoneNumbers() {
        return phoneNumbers;
    }

    public void setPhoneNumbers(Map<String, String> phoneNumbers) {
        this.phoneNumbers = phoneNumbers;
    }

    public List<Address> getPrevAddresses() {
        return prevAddresses;
    }

    public void setPrevAddresses(List<Address> prevAddresses) {
        this.prevAddresses = prevAddresses;
    }

    public Credential getCredential() {
        return credential;
    }

    public void setCredential(Credential credential) {
        this.credential = credential;
    }

    @Override
    public String toString() {
        return "User{" + "id=" + id + ", name=" + name + ", createdDate=" + createdDate + ", birthDay=" + birthDay + ", age=" + age + ", valid=" + valid + ", postAddress=" + postAddress + ", bankAddress=" + bankAddress + ", emails=" + emails + ", phoneNumbers=" + phoneNumbers + ", prevAddresses=" + prevAddresses + ", credential=" + credential + '}';
    }
    

   
}
